import java.util.EnumMap;
import java.util.Map;

/**
 * Sorting of balls.
 *
 * @since 1.8
 */
public class ColorSort {

    enum Color {red, green, blue}

    public static void main(String[] param) {
        // for debugging
    }

    //RGB order
    public static void reorder(Color[] balls) {

        Map<Color, Integer> counterDict = new EnumMap<Color, Integer>(Color.class);

        counterDict.put(Color.red, 0);
        counterDict.put(Color.green, 0);
        counterDict.put(Color.blue, 0);

        for (Color ball : balls) {
            counterDict.put(ball, counterDict.get(ball) + 1);
        }

        int counter = 0;
        for (Color name : Color.values()) {
            for (int i = 0; i < counterDict.get(name); i++) {
                balls[counter] = name;
                counter++;
            }
        }
    }
}
